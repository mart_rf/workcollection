﻿namespace TeamPicker
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Xml.Linq;
    using Facet.Combinatorics;
    internal class DataManager
    {
        private ObservableCollection<string> log { get; set; }

        public DataManager(ObservableCollection<string> log)
        {
            this.log = log;
        }

        public IEnumerable<Player> ReadData()
        {
            XElement[] players;
            try
            {
                XDocument doc = XDocument.Load("data.xml");
                players = doc.Root.Descendants().ToArray();
            }
            catch (Exception ex)
            {
                log.Add($"EXCEPTION when reading data: {ex.Message}");
                yield break;
            }

            foreach (var player in players)
            {
                yield return new Player()
                {
                    Plays = true,
                    Name = player.Attribute("Name").Value,
                    Score = double.Parse(player.Attribute("Score").Value, CultureInfo.InvariantCulture)
                };
            }
        }

        public void SaveData(IEnumerable<Player> players)
        {
            XDocument doc = new XDocument(
                new XElement("players",
                    players.Select(s => new XElement("player",
                        new XAttribute("Name", s.Name),
                        new XAttribute("Score", s.Score.ToString(CultureInfo.InvariantCulture))))));
            doc.Save("data.xml");

        }

        public void CreateSubset(IList<Player> playersToPlay, PickMethod pickMethod)
        {
            switch (pickMethod)
            {
                case PickMethod.Random:
                    RandomSubset(playersToPlay);
                    break;
                case PickMethod.Best:
                    BruteForceSubset(playersToPlay);
                    break;
                default:
                    break;
            }
        }

        private void BruteForceSubset(IList<Player> playersToPlay)
        {
            var n = playersToPlay.Count;

            var permutations = new Permutations<int>(Enumerable.Range(0, n).ToList(), GenerateOption.WithoutRepetition);

            var bestScoreDiff = double.MaxValue;
            IList<int> bestScorePosibility = null;

            foreach (IList<int> posibility in permutations)
            {            
                var teamAScore = 0d;
                var teamBScore = 0d;
                

                bool flip = true;
                foreach (var item in posibility)
                {
                    if (flip)
                    {
                        teamAScore += playersToPlay[item].Score;
                    }
                    else
                    {
                        teamBScore += playersToPlay[item].Score;
                    }
                    flip = !flip;
                }

                var currentDiff = Math.Abs(teamAScore - teamBScore);

                if (bestScoreDiff > currentDiff)
                {
                    bestScoreDiff = currentDiff;
                    bestScorePosibility = posibility;
                }
            }

            bool bestFlip = true;
            var teamA = new List<Player>();
            var teamB = new List<Player>();

            foreach (var item in bestScorePosibility)
            {
                if (bestFlip)
                {
                    teamA.Add(playersToPlay[item]);
                }
                else
                {
                    teamB.Add(playersToPlay[item]);
                }
                bestFlip = !bestFlip;
            }


            OutputSubsets(teamA, teamB);
        }



        private void RandomSubset(IList<Player> playersToPlay)
        {
            var teamA = new List<Player>();
            var teamB = new List<Player>();
            var teamASum = 0d;
            var teamBSum = 0d;

            while (playersToPlay.Any())
            {
                var diff = Math.Abs(teamASum - teamBSum);
                Player player;

                if (diff > 0)
                {
                    var subset = playersToPlay.OrderBy(p => Math.Abs(p.Score - diff)).Take(3).ToList().Shuffle();
                    player = subset.First();
                }
                else
                {
                    player = playersToPlay.OrderByDescending(p => p.Score).First();
                }

                playersToPlay.Remove(player);

                if (teamA.Count <= teamB.Count)
                {

                    teamA.Add(player);
                    teamASum += player.Score;
                }
                else
                {
                    teamB.Add(player);
                    teamBSum += player.Score;
                }
            }

            OutputSubsets(teamA, teamB);
        }

        private void OutputSubsets(IEnumerable<Player> teamA, IEnumerable<Player> teamB)
        {
            log.Add($"TEAM A:\t({teamA.Sum(s => s.Score).ToString("0.#")})");
            OutputTeam(teamA);
            log.Add($"TEAM B:\t({teamB.Sum(s => s.Score).ToString("0.#")})");
            OutputTeam(teamB);
        }

        private void OutputTeam(IEnumerable<Player> team)
        {
            foreach (var player in team)
            {
                log.Add($"\t{player.Name}");
            }
        }
    }
}

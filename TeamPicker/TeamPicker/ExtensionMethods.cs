﻿namespace TeamPicker
{
    using System.Collections.Generic;
    using System.Linq;

    internal static class ExtensionMethods
    {
        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }        
    }
}

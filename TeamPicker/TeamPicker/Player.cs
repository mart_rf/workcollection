﻿namespace TeamPicker
{
    using System.ComponentModel;
    using System.Diagnostics;

    [DebuggerDisplay("Player: {Name} ({Score})")]
    public class Player : INotifyPropertyChanged
    {
        private bool plays;

        public bool Plays
        {
            get { return plays; }
            set
            {
                this.plays = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Plays)));
            }
        }
        public string Name { get; set; }
        public double Score { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public override bool Equals(object obj)
        {
            return obj != null && obj is Player && ((Player)obj).Name == this.Name;
        }

        public override int GetHashCode()
        {
            return this.Name != null ? this.Name.GetHashCode() : 0;
        }
    }
}

﻿namespace TeamPicker
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public BindingList<Player> Players { get; set; }
        public ObservableCollection<string> Log { get; set; }
        private DataManager DataManager { get; set; }
        private bool AutoScroll { get; set; }

        public int PlayersCount
        {
            get
            {
                return this.Players.Count(c => c.Plays);
            }
        }

        public MainWindow()
        {
            Players = new BindingList<Player>();
            Players.ListChanged += Players_ListChanged;
            Players.AddingNew += Players_AddingNew;            
            Log = new ObservableCollection<string>();
            Log.CollectionChanged += Log_CollectionChanged;
            DataManager = new DataManager(Log);

            InitializeComponent();
        }

        private void Players_AddingNew(object sender, AddingNewEventArgs e)
        {
            numberOfPlayersValueLevel.Content = PlayersCount;
        }

        private void Players_ListChanged(object sender, ListChangedEventArgs e)
        {
            numberOfPlayersValueLevel.Content = PlayersCount;
        }

        private void Log_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Border border = (Border)VisualTreeHelper.GetChild(this.listBox, 0);

            var scroll = (ScrollViewer)VisualTreeHelper.GetChild(border, 0);
            if (scroll.VerticalOffset == scroll.ScrollableHeight)
            {
                AutoScroll = true;
            }
            else
            {
                AutoScroll = false;
            }

            if (AutoScroll)
            {
                scroll.ScrollToBottom();
            }
        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            this.Players.Clear();
            foreach (var player in DataManager.ReadData())
            {
                this.Players.Add(player);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            DataManager.SaveData(this.Players);
        }

        private void calculateRandomButton_Click(object sender, RoutedEventArgs e)
        {
            Log.Add("=========================================");
            IList<Player> playersToPlay = this.Players.Where(p => p.Plays).ToList().Shuffle();
            DataManager.CreateSubset(playersToPlay, PickMethod.Random);
        }

        private void calculateBestButton_Click(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;
            this.Cursor = Cursors.Wait;
            Log.Add("=========================================");
            IList<Player> playersToPlay = this.Players.Where(p => p.Plays).ToList().Shuffle();
            DataManager.CreateSubset(playersToPlay, PickMethod.Best);
            this.Cursor = null;
            this.IsEnabled = true;
        }

        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {

            if (listBox.SelectedItems != null)
            {
                string copy = string.Empty;
                foreach (string item in listBox.SelectedItems) { copy += item + "\r\n"; }

                new SetClipboardHelper(DataFormats.Text, copy).Go();
            }
        }

        private void RightClickCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            this.Log.Clear();
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            this.Players.Remove((Player)this.dataGrid.SelectedItem);
        }
    }
}

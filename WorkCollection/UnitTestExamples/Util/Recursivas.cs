﻿namespace Util
{
    using System.Diagnostics.Contracts;

    /// <summary>
    /// A recursive implementation of <see cref="IMathUtil"/>.
    /// </summary>
    /// <seealso cref="Util.IMathUtil" />
    public class Recursive : IMathUtil
    {
        /// <inheritdoc />
        public int Factorial(int n)
        {
            Contract.Requires(n > 0);
            if (n == 0) return 1;
            return n * Factorial(n - 1);
        }

        /// <inheritdoc />
        public int Exponentiation(int @base, int exponent)
        {
            Contract.Requires(exponent > 0);
            if (@base == 0) return 0;
            if (exponent == 1) return @base;
            if (exponent == 0) return 1;
            return @base * Exponentiation(@base, exponent - 1);
        }
    }
}

﻿namespace Util
{
    using System.Diagnostics.Contracts;

    /// <summary>
    /// An iterative implementation of <see cref="IMathUtil"/>.
    /// </summary>
    public class Iterative : IMathUtil
    {
        /// <inheritdoc />
        public int Factorial(int n)
        {
            Contract.Requires(n > 0);
            var aux = 1;
            for (var i = n; i > 0; i--)
            {
                aux = i * aux;
            }

            return aux;
        }

        /// <inheritdoc />
        public int Exponentiation(int @base, int exponent)
        {
            Contract.Requires(exponent > 0);
            var aux = 1;
            while (exponent > 0)
            {
                aux = aux * @base;
                exponent--;
            }

            return aux;
        }
    }
}

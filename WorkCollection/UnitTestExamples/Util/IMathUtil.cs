﻿namespace Util
{
    public interface IMathUtil
    {
        /// <summary>
        /// Factorials the specified n.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <returns></returns>
        int Factorial(int n);

        /// <summary>
        /// Exponentiations the specified base.
        /// </summary>
        /// <param name="base">The base.</param>
        /// <param name="exponent">The exponent.</param>
        /// <returns></returns>
        int Exponentiation(int @base, int exponent);        
    }
}

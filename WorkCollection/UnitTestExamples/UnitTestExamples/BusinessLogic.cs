﻿namespace UnitTestExamples
{
    using Entities;
    using Services;

    public class BusinessLogic
    {
        private readonly IUserService _userService;

        public BusinessLogic(IUserService userService) {
            _userService = userService;
        }

        public User Add(User u) {
            return _userService.Add(u);
        }        
    }
}

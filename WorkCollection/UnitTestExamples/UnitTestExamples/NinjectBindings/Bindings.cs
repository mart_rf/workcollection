﻿namespace UnitTestExamples.NinjectBindings
{
    using Ninject.Modules;
    using Services;

    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<SqlUserService>();
        }
    }
}

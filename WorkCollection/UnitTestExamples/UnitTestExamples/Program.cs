﻿namespace UnitTestExamples
{
    using System;
    using System.Reflection;
    using Entities;
    using Ninject;
    using Services;

    public class Program
    {
        public static void Main(string[] args)
        {            
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            var bl = new BusinessLogic(kernel.Get<IUserService>());
            Console.WriteLine(bl.Add(new User()).ToString());
            Console.Read();
        }
    }
}

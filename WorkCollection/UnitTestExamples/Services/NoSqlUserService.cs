﻿namespace Services
{
    using System;
    using Entities;

    public class NoSqlUserService : IUserService
    {
        /// <summary>
        /// Adds the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>The user.</returns>
        public User Add(User user)
        {
            return new User
            {
                Id = Guid.NewGuid(),
                Name = "New User",
                LastName = "No Sql Storage"
            };
        }

        /// <summary>
        /// Updates the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>The user.</returns>
        public User Update(User user)
        {
            return new User
            {
                Id = Guid.NewGuid(),
                Name = "New User",
                LastName = "No Sql Storage"
            };
        }

        /// <summary>
        /// Deletes the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>The user.</returns>
        public User Delete(User user)
        {
            return new User
            {
                Id = Guid.NewGuid(),
                Name = "New User",
                LastName = "No Sql Storage"
            };
        }
    }
}

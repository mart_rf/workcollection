﻿namespace Tests.Util
{
    using global::Util;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class RecursiveTests : UtilTests
    {
        // clase concreta hija de UtilTests que solo implementa 
        // el método abstracto declarado en su parent class
        public override IMathUtil CreateUtil()
        {
            return new Recursive();
        }
    }
}

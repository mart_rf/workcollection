﻿namespace Tests.Util
{
    using global::Util;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    // Se crea una clase abstracta para poder hacer template method en cada test de forma
    // que se verifique el mismo comportamiento en todas las implementaciones de una misma inteface
    public abstract class UtilTests
    {
        // este metodo será quien resuleva que implementación concreta sera la útilizada en runtime
        public abstract IMathUtil CreateUtil();               
        
        [TestMethod]
        public void FactorialBaseline()
        {            
            var util = CreateUtil();
            Assert.AreEqual(1, util.Factorial(0));
            Assert.AreEqual(1, util.Factorial(1));
            Assert.AreEqual(2, util.Factorial(2));
            Assert.AreEqual(6, util.Factorial(3));
            Assert.AreEqual(24, util.Factorial(4));
            Assert.AreEqual(120, util.Factorial(5));
        }

        [TestMethod]
        public void ExponentiationBaseline()
        {
            var util = CreateUtil();
            Assert.AreEqual(0, util.Exponentiation(0, 2));
            Assert.AreEqual(1, util.Exponentiation(1, 9));
            Assert.AreEqual(9, util.Exponentiation(3, 2));
            Assert.AreEqual(64, util.Exponentiation(4, 3));
            Assert.AreEqual(243, util.Exponentiation(3, 5));
            Assert.AreEqual(78125, util.Exponentiation(5, 7));
            Assert.AreEqual(1, util.Exponentiation(12, 0));
            Assert.AreEqual(10648, util.Exponentiation(22, 3));
        }      
    }
}


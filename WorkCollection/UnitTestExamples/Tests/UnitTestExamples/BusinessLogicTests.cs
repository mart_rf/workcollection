﻿namespace Tests.UnitTestExamples
{
    using System;
    using Entities;
    using global::UnitTestExamples;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Services;

    [TestClass]
    public class BusinessLogicTests
    {        
        [TestMethod]
        public void AddUser() {
            // creo los mocks que necesita la clase a testear
            var mockServiceUser = new Mock<IUserService>();
            // elemento de retorno a usar en el setup del metodo a usar
            var returnUser = new User
            {
                Id = Guid.Empty,
                Name = "Test Name",
                LastName = "Test Last Name"
            };
            // intancia de la clase a testear
            var bl = new BusinessLogic(mockServiceUser.Object);
            // setup del metodo en el objeto mockeado y seteo del retorno del metodo
            // para cuando es llamado con cualquier parametro de usuario
            mockServiceUser.Setup(m => m.Add(It.IsAny<User>())).Returns(returnUser);
            // ejecuto el metodo a testear
            var result = bl.Add(new User ());            
            // verificacion de que se llamo una sola vez al método mockeado y que los parametros fueron 
            // los correctos
            mockServiceUser.Verify(c => c.Add(It.Is<User>(
                u => u.Id == Guid.Empty && string.IsNullOrEmpty(u.Name)
                    && string.IsNullOrEmpty(u.LastName)
               )), Times.Once());
            // Assers para verificar que la respuesta es la misma que devuelve el servicio
            Assert.AreEqual(result.Id, returnUser.Id);
            Assert.AreEqual(result.Name, returnUser.Name);
            Assert.AreEqual(result.LastName, returnUser.LastName);
        }
    }
}

﻿namespace Entities
{
    using System;

    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }

        public override string ToString()
        {
            return "ID: " + Id + " Name: " + Name + " Last Name: " + LastName;
        }
    }
}

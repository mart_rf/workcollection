﻿namespace LnLTest
{
    using System;
    using LnLTDD;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class StringCalculatorTest
    {
        private StringCalculator _calc = new StringCalculator();

        [TestMethod]
        public void StringEmptyZero()
        {
            Assert.AreEqual(0, _calc.Add(string.Empty));
        }

        [TestMethod]
        public void OneEqualsOne()
        {
            Assert.AreEqual(1, _calc.Add("1"));
        }

        [TestMethod]
        public void OneTwoEqualsThree()
        {
            Assert.AreEqual(3, _calc.Add("1, 2"));
        }

        [TestMethod]
        public void FiveArguments()
        {
            Assert.AreEqual(134, _calc.Add("1, 2, 33, 8, 90"));
        }

        [TestMethod]
        public void OneToFourteenEquals105()
        {
            Assert.AreEqual(105, _calc.Add("1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14"));
        }

        [TestMethod]
        public void AllowNewLinesBetweenNumbers()
        {
            Assert.AreEqual(105, _calc.Add("1\n 2\n 3, 4\n 5\n 6\n 7\n 8\n 9, 10\n 11\n 12\n 13\n 14"));
        }

        [TestMethod]
        public void OnlyNewLinesBetweenNumbers()
        {
            Assert.AreEqual(105, _calc.Add("1\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n 9\n 10\n 11\n 12\n 13\n 14"));
        }

        [TestMethod]
        public void SupportDifferentDelimiters()
        {
            Assert.AreEqual(105, _calc.Add("//;\n1\n 2\n 3; 4\n 5\n 6\n 7\n 8\n 9; 10\n 11\n 12\n 13\n 14"));
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentException))]
        public void NegativesNotAllowed()
        {
            Assert.AreEqual(105, _calc.Add("1, 2, 3, 4, 5, -6, 7, 8, 9, 10, -11, 12, 13, 14"));
        }

        [TestMethod]
        public void IgnoreNumbersBiggerThan1000()
        {
            Assert.AreEqual(2, _calc.Add("2, 1001"));
        }
    }
}

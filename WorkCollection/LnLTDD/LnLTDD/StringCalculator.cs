﻿namespace LnLTDD
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class StringCalculator
    {

        //public int Add(string numbers)
        //{
        //    if (string.IsNullOrEmpty(numbers)) return 0;
        //    return 1;
        //}

        //public int Add(string numbers)
        //{
        //    if (string.IsNullOrEmpty(numbers)) return 0;
        //    return numbers.Split(',').Sum(num => int.Parse(num));
        //}


        //public int Add(string numbers)
        //{
        //    if (string.IsNullOrEmpty(numbers)) return 0;
        //    var delimiters = new List<char> {',', '\n'};
        //    return numbers.Split(delimiters.ToArray()).Sum(num => int.Parse(num));
        //}

        //public int Add(string numbers)
        //{
        //    if (string.IsNullOrEmpty(numbers)) return 0;
        //    var delimiters = Delimiters;
        //    numbers = ParseCustomDelimiter(numbers, delimiters);
        //    if (numbers.Split(delimiters.ToArray()).Any(i => int.Parse(i) < 0))
        //    {
        //        throw new ArgumentException("Negatives not allowed.");
        //    }
        //    return numbers.Split(delimiters.ToArray()).Sum(num => int.Parse(num));
        //}

        //public int Add(string numbers)
        //{
        //    if (string.IsNullOrEmpty(numbers)) return 0;
        //    var delimiters = Delimiters;
        //    numbers = ParseCustomDelimiter(numbers, delimiters);
        //    var numbersArray = numbers.Split(delimiters.ToArray());
        //    if (numbersArray.Any(i => int.Parse(i) < 0))
        //    {
        //        throw new ArgumentException("Negatives not allowed.");
        //    }
        //    return numbersArray.Sum(num => int.Parse(num));
        //}

        //public int Add(string numbers)
        //{
        //    if (string.IsNullOrEmpty(numbers)) return 0;
        //    var delimiters = Delimiters;
        //    numbers = ParseCustomDelimiter(numbers, delimiters);
        //    var numbersArray = numbers.Split(delimiters.ToArray());
        //    if (numbersArray.Any(i => int.Parse(i) < 0))
        //    {
        //        throw new ArgumentException("Negatives not allowed.");
        //    }
        //    numbersArray = IgnoreNumbersBiggerThan1000(numbersArray);
        //    return numbersArray.Sum(num => int.Parse(num));
        //}

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers)) return 0;
            var delimiters = Delimiters;
            numbers = ParseCustomDelimiter(numbers, delimiters);
            var numbersArray = numbers.Split(delimiters.ToArray());
            if (numbersArray.Any(i => int.Parse(i) < 0))
            {
                throw new ArgumentException("Negatives not allowed.");
            }
            numbersArray = IgnoreNumbersBiggerThan1000(numbersArray);
            return numbersArray.Sum(num => int.Parse(num));
        }

        private static string[] IgnoreNumbersBiggerThan1000(string[] numbersArray)
        {
            numbersArray = numbersArray.Where(x => int.Parse(x) <= 1000).ToArray();
            return numbersArray;
        }


        private static string ParseCustomDelimiter(string numbers, List<char> delimiters)
        {
            if (!numbers.StartsWith(CustomDelimiterToken)) return numbers;
            delimiters.Add(numbers[2]);
            numbers = numbers.Substring(4);
            return numbers;
        }

        private static string CustomDelimiterToken
        {
            get { return "//"; }
        }

        private static List<char> Delimiters
        {
            get
            {
                var delimiters = new List<char> {DefaultDelimiter, NewLineDelimiter};
                return delimiters;
            }
        }

        private static char NewLineDelimiter
        {
            get { return '\n'; }
        }

        private static char DefaultDelimiter
        {
            get { return ','; }
        }
    }
}

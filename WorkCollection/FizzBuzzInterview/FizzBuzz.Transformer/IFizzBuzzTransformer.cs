﻿namespace FizzBuzz.Transformer
{
    using System.Collections.Generic;

    public interface IFizzBuzzTransformer
    {
        IEnumerable<string> Transform(IEnumerable<int> sequence);
    }
}

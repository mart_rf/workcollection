﻿namespace FizzBuzz.TransformerTest
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Transformer;

    /// <summary>
    /// Summary description for FizzBuzzTransformerTest
    /// </summary>
    [TestClass]
    public class FizzBuzzTransformerTest
    {
        private IFizzBuzzTransformer _transformer;

        [TestInitialize]
        public void Setup()
        {
            this._transformer = new FizzBuzzTransformer();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NotASequence()
        {
            int[] sequence = { -2, 3, 6 };
            this._transformer.Transform(sequence);
        }

        [TestMethod]
        public void OneToThree()
        {
            int[] sequence = { 1, 2, 3 };
            var result = this._transformer.Transform(sequence);
            string[] witness = { "1", "2", "Fizz" };
            CollectionAssert.AreEqual(witness, result.ToArray());
        }

        [TestMethod]
        public void OneToFive()
        {
            int[] sequence = { 1, 2, 3, 4, 5 };
            var result = this._transformer.Transform(sequence);
            string[] witness = { "1", "2", "Fizz", "4", "Buzz" };
            CollectionAssert.AreEqual(witness, result.ToArray());
        }

        [TestMethod]
        public void OneToFifteen()
        {
            var sequence = Enumerable.Range(1, 15).ToArray();
            var result = this._transformer.Transform(sequence);
            string[] witness = { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz" };
            CollectionAssert.AreEqual(witness, result.ToArray());
        }
    }
}

﻿namespace FizzBuzz.Console
{
    using System.Collections.Generic;
    using System.Linq;
    using Transformer;

    class Program
    {
        static void Main(string[] args)
        {
            var transformer = TransformerFactory("FizzBuzz");
            PrintTransformation(transformer.Transform(Enumerable.Range(1, 100).ToArray()));
            System.Console.ReadLine();
        }

        private static void PrintTransformation(IEnumerable<string> transform)
        {
            foreach (var item in transform)
            {
                System.Console.WriteLine(item);
            }
        }

        private static IFizzBuzzTransformer TransformerFactory(string fizzbuzz)
        {
            switch (fizzbuzz)
            {
                case "FizzBuzz":
                    return new FizzBuzzTransformer();
            }

            return null;
        }
    }
}
